package cours.src;

public class App {
  /**
   * Simple String manipulations
   * 
   * @param args
   */
  public static void main(String[] args) {
    /*
     * System.out.println("Hello, World!"); System.out.println("Tests");
     * System.out.println("*******"); String prenom = "Sherlock"; String nom =
     * "Holmes"; System.out.println(prenom + " " + nom);
     * 
     * System.out.println(nom.charAt(0)); char lastLetter = nom.charAt(nom.length()
     * - 1); System.out.println(lastLetter);
     * 
     * String lastLetterToString = lastLetter + "";
     * 
     * System.out.println(lastLetterToString.toUpperCase());
     * System.out.println("*******"); String sample =
     * "This is a very good sample String";
     * 
     * String[] sampleSplit = sample.split(" ");
     * 
     * for (String string : sampleSplit) { if (string.equals("good")) {
     * System.out.println(string.toUpperCase()); } else {
     * System.out.println(string); } } System.out.println("*******");
     */
    // Prog Objet
    /*
     * Personne p = new Personne("Blanc", "Michel", 182);
     * System.out.println(p.getFullname()); p.setNom(nom); p.setPrenom(prenom);
     * System.out.println(p.getFullname()); System.out.println("Il fait " +
     * p.getTaille() + "cm"); System.out.println(p.toString());
     * System.out.println("*******");
     */

    String s = "Amalvy Theo 170;Cabrer Aloïs 181;Clerc Thomas 170;";

    String[] splittedS = s.split(";");
    Personne[] persTab = new Personne[splittedS.length];
    for (int i = 0; i < splittedS.length; i++) {
      String[] attributs = splittedS[i].split(" ");
      persTab[i] = new Personne(attributs[0].toUpperCase(), attributs[1], Integer.parseInt(attributs[2]));
    }

    for (Personne personne : persTab) {
      System.out.println(personne);
    }
  }
}

package cours.src;

public class Personne {
  // Attribute
  private String nom;
  private String prenom;
  private int taille;

  // Constructors
  public Personne(String setupNom, String setupPrenom, int setupTaille) {
    this.nom = setupNom;
    this.prenom = setupPrenom;
    this.taille = setupTaille;
  }


  // Functions
  public String getNom() {
    return nom;
  }

  public String getPrenom() {
    return prenom;
  }

  public int getTaille() {
    return taille;
  }


  public void setNom(String newNom) {
    this.nom = newNom;
  }

  public void setPrenom(String newPrenom) {
    this.prenom = newPrenom;
  }

  public String getFullname() {
    return prenom + " " + nom;
  }

  public void setTaille(int newTaille) {
    this.taille = newTaille;
  }


  @Override
  public String toString() {
    return "{" +
      " nom='" + getNom() + "'" +
      ", prenom='" + getPrenom() + "'" +
      ", taille='" + getTaille() + "'" +
      "}";
  }


}
